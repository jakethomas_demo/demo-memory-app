//
//  Game.swift
//  Demo Memory Game
//
//  Created by Jake Thomas on 5/17/22.
//

import Combine
import SwiftUI

class Game: ObservableObject {
    
    @Published fileprivate var refreshScreen = true
    
    let player: String
    fileprivate var scoreKeeper = ScoreKeeper()
    
    // Classes, including `Card`, are reference types.
    // Therefore, `RefRef` is a reference to a reference.
    // Needed so a `Card` can make itself the current card
    // and communicate that to the next `Card` to be selected.
    // (Passing a `Card` makes a copy of the reference;
    // we need to mutate that reference instead.)
    class RefRef {
        var card: Card?
    }
    private let currentCardRef = RefRef()
    
    fileprivate var deck: [Card]
    
    static let cardCount = 30
    
    init(player: String) {
        self.player = player
        deck = []
        initDeck()
    }
    
    private func initDeck() {
        deck = DeckMaker.makeDeck(
            scoreKeeper: scoreKeeper,
            currentCardRef: currentCardRef
        )
    }
    
    func reset() {
        scoreKeeper = ScoreKeeper()
        currentCardRef.card = nil
        initDeck()
    }
}

struct GameView: View {
    
    // 🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨🚨
    // Before adding `@ObservedObject`, I had to go back
    // to "Pick a Game" and then back in for the card to
    // redraw (flip to show obverse).
    // https://stackoverflow.com/a/62919526/16771222 (Block at top)
    @ObservedObject private var game: Game
    
    var body: some View {
        // Each item in `rows` is a template used to build out a given row,
        // but the item does not actually occur in the grid; it's only a template.
        // Because `rows` has 5 elements, there's 5 rows:
        LazyHGrid(
            rows: [GridItem](repeating: GridItem(.flexible(minimum: 50, maximum: 100)), count: 5),
            alignment: .center,
            spacing: 10
        ) {
            // Here, we loop over all cards.
            // Note: 30 cards ÷ 5 rows = 6 columns.
            ForEach(game.deck, id: \.self) { card in
                Button(action: refresh(after: card.onTap)) {
                    HStack {
                        Spacer()
                            .frame(width: 30)
                        Text(card.upSide)
                            .font(.system(size: 50))
                        Spacer()
                            .frame(width: 30)
                    }
                    .contentShape(Rectangle())
                    .background(Color(UIColor(named: "Card")!))
                    .cornerRadius(8)
                }
            }
        }
        // `inline` gets rid of annoying top whitespace.
        // See analog in `Login`, where `inline` must go
        // in a different spot.
        .navigationBarTitleDisplayMode(.inline)
        .navigationTitle("\(game.player)’s Game")
        
        HStack {
            Button("Reset Game", action: refresh(after: game.reset))
            Spacer().frame(minWidth:200, maxWidth: 300)
            Text(game.scoreKeeper.memoryRateMsg)
        }
    }
    
    init(player: String) {
        let game = Game(player: player)
        self.game = game
    }
    
    typealias PlainAction = () -> ()
    private func refresh(after action: @escaping PlainAction) -> PlainAction {
        return {
            action()
            game.refreshScreen = !game.refreshScreen
        }
    }
}

struct GameView_Previews: PreviewProvider {
    static var previews: some View {
        GameView(player: "Player")
    }
}
