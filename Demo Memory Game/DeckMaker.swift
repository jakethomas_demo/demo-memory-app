//
//  DeckMaker.swift
//  Demo Memory Game
//
//  Recommended: https://getemoji.com/
//  Originally used https://www.freecodecamp.org/news/all-emojis-emoji-list-for-copy-and-paste/,
//  but had joiner problems copying and pasting the person emojis. Would use getemoji.com 1st
//  next time.
//
//  Created by Jake Thomas on 5/21/22.
//

import SwiftUI

struct DeckMaker {

    static private var emojiStr =
    
        // Smiley Face Emojis
        "🙂😀😃😄😁😅😆🤣😂🙃😉😊😇😎🤓🧐🥳" +

        // Emotional Faces
        //☺
        "🥰😍🤩😘😗😚😙🥲" +

        // Faces with Tongue Emojis
        "😋😛😜🤪😝🤑" +

        // Faces with Hands Emojis
        "🤗🤭🤫🤔" +

        // Neutral Faces Emojis
        "😐🤐🤨😑😶😏😒🙄😬😮‍💨🤥" +

        // Sleepy Faces Emojis
        "😪😴😌😔🤤" +

        // Sick Faces Emojis
        "😷🤒🤕🤢🤮🤧🥵🥶🥴😵🤯" +

        // Concerned Faces Emojis
        //☹
        "😕😟🙁😮😯😲😳🥺😦😧😨😰😥😢😭😱😖😣😞😓😩😫🥱" +

        //Negative Faces
        //😤😡😠🤬😈👿💀☠

        //Costume Faces Emojis
        //💩
        "🤡👹👺👻👽👾🤖" +

        // Cat Faces Emojis
        "😺😸😹😻😼😽🙀😿😾" +

        // Monkey Faces Emojis
        "🙈🙉🙊" +

        // Emotion Emojis
        "💋💌💘💝💖💗💓💞💕💟❣💔❤️‍🔥❤️‍🩹❤🧡💛💚💙💜🤎🖤🤍💯💢💥💫💦💨🕳💣💬👁️‍🗨️🗨🗯💭💤" +

        // Hands and other Body Parts Emojis
        //🦻
        "👋🤚🖐✋🖖👌🤌🤏✌🤞🤟🤘🤙👈👉👆👇☝🫵👍👎✊👊🤛🤜👏🙌👐🤲🤝🙏✍💅🤳💪🦾🦵🦿🦶👂👃🧠👣🫀🫁🦷🦴👀👁👅👄" +

        // Person Emojis
        "👶👧🧒👦👩🧑👨👩‍🦱🧑‍🦱👨‍🦱👩‍🦰🧑‍🦰👨‍🦰👱‍♀️👱👱‍♂️👩‍🦳🧑‍🦳👨‍🦳👩‍🦲🧑‍🦲👨‍🦲🧔👵🧓👴👲👳‍♀️👳👳‍♂️🧕👮‍♀️👮👮‍♂️👷‍♀️👷👷‍♂️💂‍♀️💂💂‍♂️🕵️‍♀️🕵️🕵️‍♂️👩‍⚕️🧑‍⚕️👨‍⚕️👩‍🌾🧑‍🌾👨‍🌾👩‍🍳🧑‍🍳👨‍🍳👩‍🎓🧑‍🎓👨‍🎓👩‍🎤🧑‍🎤👨‍🎤👩‍🏫🧑‍🏫👨‍🏫👩‍🏭🧑‍🏭👨‍🏭👩‍💻🧑‍💻👨‍💻👩‍💼🧑‍💼👨‍💼👩‍🔧🧑‍🔧👨‍🔧👩‍🔬🧑‍🔬👨‍🔬👩‍🎨🧑‍🎨👨‍🎨👩‍🚒🧑‍🚒👨‍🚒👩‍✈️🧑‍✈️👨‍✈️👩‍🚀🧑‍🚀👨‍🚀👩‍⚖️🧑‍⚖️👨‍⚖️👰‍♀️👰👰‍♂️🤵‍♀️🤵🤵‍♂️👸🤴🥷🦸‍♀️🦸🦸‍♂️🦹‍♀️🦹🦹‍♂️🤶🧑‍🎄🎅🧙‍♀️🧙🧙‍♂️🧝‍♀️🧝🧝‍♂️🧛‍♀️🧛🧛‍♂️🧟‍♀️🧟🧟‍♂️🧞‍♀️🧞🧞‍♂️🧜‍♀️🧜🧜‍♂️🧚‍♀️🧚🧚‍♂️👼🤰🤱👩‍🍼🧑‍🍼👨‍🍼🙇‍♀️🙇🙇‍♂️💁‍♀️💁💁‍♂️🙅‍♀️🙅🙅‍♂️🙆‍♀️🙆🙆‍♂️🙋‍♀️🙋🙋‍♂️🧏‍♀️🧏🧏‍♂️🤦‍♀️🤦🤦‍♂️🤷‍♀️🤷🤷‍♂️🙎‍♀️🙎🙎‍♂️🙍‍♀️🙍🙍‍♂️💇‍♀️💇💇‍♂️💆‍♀️💆💆‍♂️🧖‍♀️🧖🧖‍♂️💅🤳💃🕺👯‍♀️👯👯‍♂️🕴👩‍🦽🧑‍🦽👨‍🦽👩‍🦼🧑‍🦼👨‍🦼🚶‍♀️🚶🚶‍♂️👩‍🦯🧑‍🦯👨‍🦯🧎‍♀️🧎🧎‍♂️🏃‍♀️🏃🏃‍♂️🧍‍♀️🧍🧍‍♂️👭🧑‍🤝‍🧑👬👫👩‍❤️‍👩💑👨‍❤️‍👨👩‍❤️‍👨👩‍❤️‍💋‍👩💏👨‍❤️‍💋‍👨👩‍❤️‍💋‍👨👪👨‍👩‍👦👨‍👩‍👧👨‍👩‍👧‍👦👨‍👩‍👦‍👦👨‍👩‍👧‍👧👨‍👨‍👦👨‍👨‍👧👨‍👨‍👧‍👦👨‍👨‍👦‍👦👨‍👨‍👧‍👧👩‍👩‍👦👩‍👩‍👧👩‍👩‍👧‍👦👩‍👩‍👦‍👦👩‍👩‍👧‍👧👨‍👦👨‍👦‍👦👨‍👧👨‍👧‍👦👨‍👧‍👧👩‍👦👩‍👦‍👦👩‍👧👩‍👧‍👦👩‍👧‍👧🗣👤👥🫂" +

        // Animals and Nature Emojis
        "🐵🐒🦍🦧🐶🐕🦮🐕‍🦺🐩🐺🦊🦝🐱🐈🐈‍⬛🦁🐯🐅🐆🐴🐎🦄🦓🦌🦬🐮🐄🐂🐃🐷🐖🐗🐽🐏🐑🐐🐪🐫🦙🦒🐘🦣🦏🦛🐭🐁🐀🐹🐰🐇🐿🦫🦔🦇🐻🐻‍❄️🐨🐼🦥🦦🦨🦘🦡🐾🦃🐔🐓🐣🐤🐥🐦🐧🕊🦅🦆🦢🦉🦤🪶🦩🦜🐸🐊🐢🦎🐍🐲🐉🦕🦖🐳🐋🐬🦭🐟🐠🐡🦈🐙🐚🐌🦋🐛🐜🐝🪲🐞🦗🪳🕷🕸🦂🦟🪰🪱🦠💐🌸💮🏵🌹🥀🌺🌻🌼🌷🌱🪴🌲🌳🌴🌵🌾🌿☘🍀🍁🍂🍃🪴🪴" +

        // Food and Drinks Emojis
        "🍇🍈🍉🍊🍋🍌🍍🥭🍎🍏🍐🍑🍒🍓🫐🥝🍅🫒🥥🥑🍆🥔🥕🌽🌶🫑🥒🥬🥦🧄🧅🍄🥜🫑🌰🍞🥐🥖🫓🥨🥯🥞🧇🧀🍖🍗🥩🥓🍔🍟🍕🌭🥪🌮🌯🫔🥙🧆🥚🍳🥘🍲🫕🥣🥗🍿🧈🧂🥫🍱🍘🍙🍚🍛🍜🍝🍠🍢🍣🍤🍥🥮🍡🥟🥠🥡🦀🦞🦐🦑🦪🍨🍧🍦🍩🍪🎂🍰🧁🥧🍫🍬🍭🍮🍯🍼🥛☕🫖🍵🍶🍾🍷🍸🍹🍺🍻🥂🥃🥤🧋🧃🧉🧊🥢🍽🍴🥄🔪🧋🏺" +

        // Travel and Places Emojis
        //🏢🏣🏤🏥🏦🏨🏩🏪🏫🏬🏭💒🌁🌃🏙🌅🌄🌆🌇🌉♨🏞
        "🌍🌎🌏🌐🗺🧭⛰🏔🌋🗻🏕🏖🏜🏝🏟🏛🏗🧱🪨🪵🛖🏘🏚🏠🏡🗼🗽⛪🕌🛕🕍⛩🕋⛲⛺🎠🎡🎢💈🎪🏯🏰" +

        // Transport Emojis
        //🚈🚅
        "🚂🚃🚄🚆🚇🚉🚊🚝🚞🚋🚌🚍🚎🚐🚑🚒🚓🚔🚕🚖🚗🚘🚙🛻🚚🚛🚜🏎🏍🛵🦽🦼" +

        // Time Emojis
        "⌛⏳⌚⏰⏱⏲🕰" +
        //🕛🕧🕐🕜🕑🕝🕒🕞🕓🕟🕔🕠🕕🕡🕖🕢🕗🕣🕘🕤🕙🕥🕚🕦

        // Sky and Weather Emojis
        //🌠🌌🌫☃🌥
        "🌑🌒🌓🌔🌕🌖🌗🌘🌙🌚🌛🌜🌡☀🌝🌞🪐⭐🌟☁⛅⛈🌤🌦🌧🌨🌩🌪🌬🌀🌈🌂☂☔⛱⚡❄⛄☄🔥💧🌊" +

        // Activity Emojis
        //🎆🎇🎑
        "🎃🎄🧨✨🎈🎉🎊🎋🎍🎎🎏🧧🎀🎁🎗🎟🎫" +

        // Award Medals Emojis
        "🎖🏆🏅🥇🥈🥉" +

        // Sport Emojis
        "⚽⚾🥎🏀🏐🏈🏉🎾🥏🎳🏏🏑🏒🥍🏓🏸🥊🥋🥅⛳⛸🎣🤿🎽🎿🛷🥌" +

        // Games Emojis
        //🧿
        "🎯🪀🪁🎱🔮🪄🎮🕹🎰🎲🧩🧸🪅🪆♠♥♣♟🃏🀄🎴" +

        //Arts and Crafts Emojis
        "🎭🖼🎨🧵🪡🧶🪢" +

        // Clothing Objects Emojis
        "👓🕶🥽🥼🦺👔👕👖🧣🧤🧥🧦👗👘🥻🩱🩲🩳👙👚👛👜👝🛍🎒🩴👞👟🥾🥿👠👡🩰👢👑👒🎩🎓🧢🪖⛑📿💄💍💎" +

        // Sound Emojis
        //🔇🔈🔉🔊📢🔕
        "📣📯🔔🎼🎵🎶🎙🎚🎛🎤🎧📻" +

        // Musical Instrument Emojis
        "🎷🪗🎸🎹🎺🎻🪕🥁🪘" +

        // Phone Emojis
        //📱📲☎📞📟📠Computer Emojis🔋🪫🔌💻🖥🖨⌨🖱🖲💽💾💿📀🧮

        // Light and Video Emojis
        "🎥🎞📽🎬📺📷📸📹📼🔍🔎🕯💡🔦🏮🪔" +

        // Book and Paper Emojis
        "📕📖📗📘📙📚📓📒📃📜📄📰🗞📑🔖🏷💰" +
        //📔🪙💴💵💶💷💸💳🧾💹

        // Mail Emojis
        //✉📧📩📤📥📦📫📪📬📭📮🗳

        // Writing
        //✏✒
        "🖋🖊🖌🖍📝" +

        // Office Emojis
        //📂📆
        "💼📁🗂📅📇📈📉📊📋📌📍📎🖇📏📐✂🗃🗄🗑" +

        // Lock Emojis
        "🔒🔓🔏🔐🔑🗝" +

        // Tools Emojis
        "🔨🪓⛏⚒🛠🗡⚔🔫🪃🏹🛡🪚🔧🪛🔩⚙🗜⚖🦯🔗⛓🪝🧰🧲🪜" +

        // Science Emojis
        "⚗🧪🧫🧬🔬🔭📡" +

        // Medical Emojis
        "💉🩸💊🩹🩼🩺🩻" +

        // Household Emojis
        "🚪🛗🪞🪟🛏🛋🪑🚽🪠🚿🛁🪤🪒🧴🧷🧹🧺🧻🪣🧼🫧🪥🧽🧯🛒" +

        // Other Objects Emojis
        //🚬
        "⚰🪦⚱🗿🪧🪪" +

        // Symbols
        //🏧🚮🚰♿🚹🚺🚻🚼🚾🛂🛂🛄🛅

        // Warning Emojis
        //⚠🚸⛔🚫🚳🚭🚯🚱🚷📵🔞☢☣

        // Arrow Emojis
        //⬆↗➡↘⬇↙⬅↖↕↔↩↪⤴⤵🔃🔄🔙🔚🔛🔜🔝

        // Religion
        //🛐⚛🕉✡☸☯✝☦☪☮🕎🔯

        // Zodiac
        //♈♉♊♋♌♍♎♏♐♑♒♓⛎

        // AV Symbols
        //🔀🔁🔂▶⏸⏩⏭⏯◀⏪⏮🔼⏫🔽⏬⏹⏺⏏🎦🔅🔆📶📳📴

        // Gender
        //♀♂⚧

        // Math Symbols
        //✖➕➖➗🟰♾

        // Punctuation Symbols
        //‼⁉❓❔❗❕〰

        // Currency
        //💱💲

        // Other Symbols
        "🔱🔰⭕✅❌❎"
        //⚕♻⚜📛☑✔➰➿〽✳✴❇©®™

        // Keycap
        //#️⃣*️⃣0️⃣1️⃣2️⃣3️⃣4️⃣5️⃣6️⃣7️⃣8️⃣9️⃣🔟

        // Alphanum Symbols
        //🔠🔡🔢🔣🔤🅰🆎🅱🅾🆑🆒🆓ℹ🆔Ⓜ🆕🆖🆗🅿🆘🆙🆚

        // Japanese Buttons
        //🈁🈂🈷🈶🈯🉐🈹🈚🈲🉑🈸🈴🈳㊗㊙🈺🈵

        // Geometric Emojis
        //🔴🟠🟡🟢🔵🟣🟤⚫⚪🟥🟧🟨🟩🟦🟪🟫⬛⬜🔶🔷🔸🔹🔺🔻💠🔘🔳🔲

        // Flags
        //🏁🚩🎌🏴🏳🏳️‍🌈🏳️‍⚧️🏴‍☠️

        // Country Flags
        //🇦🇨🇦🇩🇦🇪🇦🇫🇦🇬🇦🇮🇦🇱🇦🇲🇦🇴🇦🇶🇦🇷🇦🇸🇦🇹🇦🇺🇦🇼🇦🇽🇦🇿🇧🇦🇧🇧🇧🇩🇧🇪🇧🇫🇧🇬🇧🇭🇧🇮🇧🇯🇧🇱🇧🇲🇧🇳🇧🇴🇧🇶🇧🇷🇧🇸🇧🇹🇧🇻🇧🇼🇧🇾🇧🇿🇨🇦🇨🇨🇨🇩🇨🇫🇨🇬🇨🇭🇨🇮🇨🇰🇨🇱🇨🇲🇨🇳🇨🇴🇨🇵🇨🇷🇨🇺🇨🇻🇨🇼🇨🇽🇨🇾🇩🇪🇩🇬🇩🇯🇩🇰🇩🇲🇩🇴🇩🇿🇪🇦🇪🇨🇪🇪🇪🇬🇪🇭🇪🇷🇪🇸🇪🇹🇪🇺🇫🇮🇫🇯🇫🇰🇫🇲🇫🇴🇫🇷🇬🇦🇬🇧🇬🇩🇬🇪🇬🇫🇬🇬🇬🇭🇬🇮🇬🇱🇬🇲🇬🇳🇬🇵🇬🇶🇬🇷🇬🇸🇬🇹🇬🇺🇬🇼🇬🇾🇭🇰🇭🇲🇭🇳🇭🇷🇭🇹🇭🇺🇮🇨🇮🇩🇮🇪🇮🇱🇮🇲🇮🇳🇮🇴🇮🇶🇮🇷🇮🇸🇮🇹🇯🇪🇯🇲🇯🇴🇯🇵🇰🇪🇰🇬🇰🇭🇰🇭🇰🇮🇰🇲🇰🇳🇰🇵🇰🇷🇰🇼🇰🇾🇰🇿🇱🇦🇱🇧🇱🇨🇱🇮🇱🇰🇱🇷🇱🇸🇱🇹🇱🇻🇱🇾🇲🇦🇲🇨🇲🇩🇲🇪🇲🇬🇲🇭🇲🇰🇲🇱🇲🇲🇲🇳🇲🇴🇲🇵🇲🇶🇲🇷🇲🇸🇲🇹🇲🇺🇲🇻🇲🇼🇲🇽🇲🇾🇲🇿🇳🇦🇳🇨🇳🇪🇳🇫🇳🇬🇳🇮🇳🇱🇳🇴🇳🇵🇳🇷🇳🇺🇳🇿🇴🇲🇵🇦🇵🇪🇵🇫🇵🇬🇵🇭🇵🇰🇵🇱🇵🇲🇵🇳🇵🇷🇵🇸🇵🇹🇵🇼🇵🇾🇶🇦🇷🇪🇷🇸🇷🇺🇷🇼🇸🇦🇸🇧🇸🇨🇸🇩🇸🇪🇸🇬🇸🇭🇸🇮🇸🇯🇸🇰🇸🇱🇸🇲🇸🇳🇸🇴🇸🇷🇸🇸🇸🇹🇸🇻🇸🇽🇸🇾🇸🇿🇹🇦🇹🇨🇹🇩🇹🇫🇹🇬🇹🇭🇹🇯🇹🇰🇹🇱🇹🇲🇹🇳🇹🇴🇹🇷🇹🇹🇹🇻🇹🇼🇹🇿🇺🇦🇺🇬🇺🇲🇺🇳🇺🇸🇺🇾🇺🇿🇻🇦🇻🇨🇻🇪🇻🇬🇻🇮🇻🇮🇻🇺🇼🇫🇼🇸🇽🇰🇾🇪🇾🇹🇿🇦🇿🇲🇿🇼

        // British Subdivision Flags
        //🏴󠁧󠁢󠁥󠁮󠁧󠁿🏴󠁧󠁢󠁳󠁣󠁴󠁿🏴󠁧󠁢󠁷󠁬󠁳󠁿
    
    static private func makeDeckData() -> (obverses: [Character], reverse: Character) {
        var emojiSet = Set(emojiStr)
        
        let reverse = emojiSet.randomElement()!
        emojiSet.remove(reverse)
        
        var obverses: [Character] = []
        for _ in 1...(Game.cardCount / 2) {
            let obverse = emojiSet.randomElement()!
            emojiSet.remove(obverse)
            obverses.append(obverse)
        }
        
        obverses = obverses + obverses
        obverses = obverses.shuffled()
        
        return (obverses, reverse)
    }
    
    static func makeDeck(
        scoreKeeper: ScoreKeeper,
        currentCardRef: Game.RefRef
    ) -> [Card] {
        
        let obverses: [Character]
        let reverse: Character
        (obverses, reverse) = Self.makeDeckData()
        
        return
            obverses.map { obverse in
                let unobservedCard =
                    Card(
                        obverse: obverse,
                        reverse: reverse,
                        scoreKeeper: scoreKeeper,
                        currentCardRef: currentCardRef
                    )
                
                @ObservedObject var card =
                    unobservedCard
                
                return card
            }
    }
}
