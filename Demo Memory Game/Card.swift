//
//  Card.swift
//  Demo Memory Game
//
//  Created by Jake Thomas on 5/20/22.
//

import SwiftUI

class Card: ObservableObject {
    
    private let obverse: Character
    private let reverse: Character
    @Published private(set) var upSide: String
    
    private let scoreKeeper: ScoreKeeper
    
    private let id = UUID()
    private let currentCardRef: Game.RefRef
    private var myPrevious: Card?
    
    init(
        obverse: Character,
        reverse: Character,
        scoreKeeper: ScoreKeeper,
        currentCardRef: Game.RefRef
    ) {
        self.obverse = obverse
        self.reverse = reverse
        self.upSide = String(reverse)
        
        self.scoreKeeper = scoreKeeper
        self.currentCardRef = currentCardRef
    }
    
    func onTap() {
        // Do nothing if user taps already-face-up card.
        guard self.upSide == String(self.reverse)
            else { return }
        
        myPrevious = currentCardRef.card
        currentCardRef.card = self
        
        reveal()
        scoreKeeper.trackMove(emoji: self.obverse)
        
        guard let myPrevious = myPrevious,
              let doublePrevious = myPrevious.myPrevious
                  else { return }
        
        let triplePrevious = doublePrevious.myPrevious
        
        if doublePrevious.obverse != myPrevious.obverse &&
           doublePrevious.obverse != triplePrevious?.obverse {
            
            doublePrevious.conceal()
        }
    }
    
    private func reveal() {
        upSide = String(obverse)
    }
    
    private func conceal() {
        upSide = String(reverse)
    }
}

extension Card: Hashable {
    static func == (lhs: Card, rhs: Card) -> Bool {
        lhs === rhs
    }
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
