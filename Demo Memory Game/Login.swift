//
//  Login.swift
//  Demo Memory Game
//
//  Created by Jake Thomas on 5/17/22.
//

import SwiftUI

struct Login: View {
    
    private var aliceGame = GameView(player: "Alice")
    private var bobGame = GameView(player: "Bob")
    
    var body: some View {
        NavigationView {
            VStack {
                Group {
                    NavigationLink(destination: aliceGame) {
                        Text("Alice’s Game")
                    }
                    
                    Spacer().frame(minHeight: 20, maxHeight: 30)
                    
                    NavigationLink(destination: bobGame) {
                        Text("Bob’s Game")
                    }
                }
                .font(.largeTitle)
            }
            .navigationBarTitle("Pick a Game")
            // `NavigationView` is a apparently a special case where
            // we have to specify `inline` on its contents (the VStack in this case),
            // rather than on itself.
            // `inline` then bubbles up to the `NavigationView`.
            // Putting `inline` directly on `NavigationView` was not successful.
            // Contrast this with `GameView` where `inline` is specified just one level in
            // from `body`.
            .navigationBarTitleDisplayMode(.inline) // Gets rid of annoying top whitespace.
        }
        .navigationViewStyle(.stack) // So we don't get a weird side thing on iPad
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Login()
    }
}
