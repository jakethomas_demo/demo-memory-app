//
//  ScoreKeeper.swift
//  Demo Memory Game
//
//  Created by Jake Thomas on 5/21/22.
//

import Combine
import SwiftUI

class ScoreKeeper: ObservableObject {
    
    private var matchesMade = 0
    private var missesMade = 0
    
    private let unmatchedEncounters = Bag()
    private var previousEmoji: Character?
    
    @Published private(set) var memoryRateMsg = "Memory rate: __%"
    private func updateMemoryRateMsg() {
        guard matchesMade != 0 || missesMade != 0 // avoid divide by zero below
            else { return }
        
        // "100%" means no "misses". See comment in `trackMove(emoji:)`.
        //
        // With luck and perfect memory, it is possible to exceed 100% when
        // never-before-seen picks for the second card so-happen to match
        // the first card.
        let percent = (matchesMade * 100) / (matchesMade + missesMade)
        memoryRateMsg = "Memory rate: \(percent)%"
    }
    
    func trackMove(emoji: Character) {
        unmatchedEncounters.add(emoji)
        
        /*
           The score only changes if a match or a miss has occurred.
           
           Suppose Bob picks "🍇", then "🕰", then
           the second "🍇" card.
         
           Now Bob picks another card. "🍇" is thus the `previousEmoji`.
              * If Bob has just now picked the other "🍇", this is a match.
              * If not, this is a miss.
           
           Either way, the previous emoji ("🍇") has been seen
           at least twice, so we're now going to update the score
           to reflect the match or miss.
        */
        if emoji == previousEmoji {
            matchesMade += 1
            unmatchedEncounters.remove(emoji)
            
            // Don't update the score onscreen for the last two cards.
            // That doesn't test the memory.
            if matchesMade < Game.cardCount / 2 {
                updateMemoryRateMsg()
            }
        }
        else if unmatchedEncounters.count(previousEmoji) >= 2 {
            missesMade += 1
            updateMemoryRateMsg()
        }

        previousEmoji = emoji
    }
}

class Bag {
    private var bag: [Character : Int] = [:]
    
    func count(_ char: Character?) -> Int {
        guard let char = char else { return 0 }
        return bag[char] ?? 0
    }
    
    func add(_ char: Character) {
        bag[char] = self.count(char) + 1
    }
    
    func remove(_ char: Character) {
        bag.removeValue(forKey: char)
    }
}
